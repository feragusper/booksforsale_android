package com.feragusper.booksforsale.domain.interactor;

import com.feragusper.booksforsale.domain.executor.PostExecutionThread;
import com.feragusper.booksforsale.domain.executor.ThreadExecutor;
import com.feragusper.booksforsale.domain.repository.BookRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * @author Fernando.Perez
 * @since 1.0
 *
 * UseCase to retrieve a list of Books
 */
public class GetBookListUseCase extends UseCase {

    public static final String CODE = GetBookListUseCase.class.getSimpleName();
    private final BookRepository bookRepository;

    @SuppressWarnings("WeakerAccess")
    @Inject
    public GetBookListUseCase(BookRepository bookRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.bookRepository = bookRepository;
    }

    @Override
    protected String makeCode() {
        return CODE;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return bookRepository.getBooks();
    }
}
