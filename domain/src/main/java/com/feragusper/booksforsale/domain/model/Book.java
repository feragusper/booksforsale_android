package com.feragusper.booksforsale.domain.model;

/**
 * @author Fernando Perez
 * @since 1.0
 */
public class Book {
    private String title;
    private String author;
    private String imageURL;

    public String getTitle() {
        return title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getAuthor() {
        return author;
    }
}
