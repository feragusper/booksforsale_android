package com.feragusper.booksforsale.domain.repository;

import com.feragusper.booksforsale.domain.model.Book;

import java.util.List;

import rx.Observable;

/**
 * @author Fernando.Perez
 * @since 1.0
 *
 * Repository to handle Books
 */
public interface BookRepository {
    Observable<List<Book>> getBooks();
}
