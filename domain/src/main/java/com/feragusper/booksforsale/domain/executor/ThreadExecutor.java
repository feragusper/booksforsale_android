package com.feragusper.booksforsale.domain.executor;

import java.util.concurrent.Executor;

/**
 * @author Fernando.Perez
 * @since 1.0
 *
 * Executor implementation can be based on different frameworks or techniques of asynchronous
 * execution, but every implementation will execute the
 * {@link com.feragusper.booksforsale.domain.interactor.UseCase } out of the UI thread.
 */
public interface ThreadExecutor extends Executor {

}
