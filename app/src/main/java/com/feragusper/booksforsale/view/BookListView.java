package com.feragusper.booksforsale.view;

import com.feragusper.booksforsale.domain.model.Book;

import java.util.List;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Interface to talk with Book List View
 */
public interface BookListView extends BaseView {
    void displayBookList(List<Book> productList);
}
