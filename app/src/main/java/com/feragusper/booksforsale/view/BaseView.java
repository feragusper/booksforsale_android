package com.feragusper.booksforsale.view;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Base Interface to talk with views of our MVP
 */
public interface BaseView {
    void showProgress();

    void hideProgress();

    void showError(String message);

    void showErrorWithRetry(String message);
}
