package com.feragusper.booksforsale;

import com.feragusper.booksforsale.data.net.Environment;

import javax.inject.Inject;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Environment to define information like API host
 */
public class EnvironmentImplementation implements Environment {

    @SuppressWarnings("WeakerAccess")
    @Inject
    public EnvironmentImplementation() {
    }

    @Override
    public String getBaseURL() {
        return BuildConfig.BASE_URL;
    }
}
