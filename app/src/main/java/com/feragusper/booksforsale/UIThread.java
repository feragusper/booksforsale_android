package com.feragusper.booksforsale;

import com.feragusper.booksforsale.domain.executor.PostExecutionThread;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Util to define which thread corresponds to UI
 */
@Singleton
public class UIThread implements PostExecutionThread {

    @SuppressWarnings("WeakerAccess")
    @Inject
    public UIThread() {
    }

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
