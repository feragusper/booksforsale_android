package com.feragusper.booksforsale.activity;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.feragusper.booksforsale.R;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Activity with one single Fragment and a Toolbar
 */
abstract class BaseFragmentActivityWithToolbar extends BaseFragmentActivity {

    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActionBar();
    }

    protected void initializeActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initializeDrawer(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getToolbarTitle());
            customizeActionBar(getSupportActionBar());
        }
    }

    protected int getLayoutResID() {
        return R.layout.activity_with_fragment_and_toolbar;
    }

    protected void customizeActionBar(ActionBar actionBar) {
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    protected void initializeDrawer(Toolbar toolbar) {

    }

    protected int getToolbarTitle() {
        return R.string.app_name;
    }

}
