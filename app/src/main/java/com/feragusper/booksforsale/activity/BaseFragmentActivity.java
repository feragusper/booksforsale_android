package com.feragusper.booksforsale.activity;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;

import com.feragusper.booksforsale.R;
import com.feragusper.booksforsale.fragment.BaseFragment;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Activity with one single Fragment
 */
abstract class BaseFragmentActivity extends BaseActivity {

    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResID());
        if (savedInstanceState == null) {
            loadFragment();
        }
    }

    protected void loadFragment() {
        BaseFragment makeFragment = makeFragment();
        String tag = makeFragment.toString();
        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            fragment = makeFragment;
            ft.add(getFragmentResIDContainer(), fragment, tag);
            ft.commit();
        }
    }

    protected abstract BaseFragment makeFragment();

    protected int getLayoutResID() {
        return R.layout.activity_with_fragment;
    }

    protected int getFragmentResIDContainer() {
        return R.id.view_container;
    }

    protected BaseFragment getFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentById(getFragmentResIDContainer());
    }
}
