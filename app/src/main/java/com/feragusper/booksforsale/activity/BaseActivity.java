package com.feragusper.booksforsale.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;

/**
 * @author Fernando Perez
 * @since 1.0
 * <p>
 * Activity with common behavior for all kind of activities used in within the app.
 */
public abstract class BaseActivity extends AppCompatActivity implements HasFragmentInjector {
    @Inject
    public DispatchingAndroidInjector<Fragment> fragmentInjector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (needToInject()) {
            AndroidInjection.inject(this);
        }
        super.onCreate(savedInstanceState);
    }

    protected boolean needToInject() {
        return true;
    }

    @Override
    public AndroidInjector<Fragment> fragmentInjector() {
        return fragmentInjector;
    }
}
