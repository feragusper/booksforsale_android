package com.feragusper.booksforsale.activity;

import com.feragusper.booksforsale.fragment.BaseFragment;
import com.feragusper.booksforsale.fragment.BookListFragment;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Activity to display a List of Books for Sale
 */
public class BookListActivity extends BaseFragmentActivity {

    @Override
    protected BaseFragment makeFragment() {
        return BookListFragment.newInstance();
    }

}