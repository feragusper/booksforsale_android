package com.feragusper.booksforsale.di.module;

import com.feragusper.booksforsale.EnvironmentImplementation;
import com.feragusper.booksforsale.UIThread;
import com.feragusper.booksforsale.data.executor.JobExecutor;
import com.feragusper.booksforsale.data.net.APIRESTClientService;
import com.feragusper.booksforsale.data.repository.BookRepositoryImplementation;
import com.feragusper.booksforsale.domain.executor.PostExecutionThread;
import com.feragusper.booksforsale.domain.executor.ThreadExecutor;
import com.feragusper.booksforsale.domain.repository.BookRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Generic Module to Inject all kind of members
 */
@Module
public class AppModule {

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    APIRESTClientService provideAPIRESTClientService(EnvironmentImplementation environment) {
        return new APIRESTClientService(environment);
    }

    @Provides
    @Singleton
    BookRepository provideBookRepository(BookRepositoryImplementation bookRepositoryImplementation) {
        return bookRepositoryImplementation;
    }

}