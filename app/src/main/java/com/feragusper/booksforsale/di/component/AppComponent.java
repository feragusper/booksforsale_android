package com.feragusper.booksforsale.di.component;

import com.feragusper.booksforsale.BooksForSaleApplication;
import com.feragusper.booksforsale.di.module.AppModule;
import com.feragusper.booksforsale.di.module.InjectorsModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Component to Inject members in the whole Application
 */
@Singleton
@Component(modules = {
        InjectorsModule.class,
        AppModule.class})
public interface AppComponent {

    void inject(BooksForSaleApplication app);

}