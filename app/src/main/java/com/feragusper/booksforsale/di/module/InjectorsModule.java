package com.feragusper.booksforsale.di.module;

import com.feragusper.booksforsale.activity.BookListActivity;
import com.feragusper.booksforsale.fragment.BookListFragment;

import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Module to Inject members to Fragments and Activities
 */
@Module(includes = AndroidInjectionModule.class)
public abstract class InjectorsModule {

    @ContributesAndroidInjector()
    abstract BookListFragment bookListFragment();

    @ContributesAndroidInjector()
    abstract BookListActivity bookListActivity();

}