package com.feragusper.booksforsale.presenter;


import com.feragusper.booksforsale.domain.interactor.GetBookListUseCase;
import com.feragusper.booksforsale.domain.model.Book;
import com.feragusper.booksforsale.view.BookListView;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Presenter to handle Book List Flow
 */
public class BookListPresenter extends BasePresenter<BookListView> {

    @SuppressWarnings("WeakerAccess")
    @Inject
    public BookListPresenter(GetBookListUseCase getProductListUseCase) {
        super(getProductListUseCase);
    }

    @Override
    public void onResume() {
        super.onResume();
        view.showProgress();
        executeUseCase(GetBookListUseCase.CODE, new GetBookListUseCaseSubscriber(this));
    }

    private void displayBookList(List<Book> bookList) {
        view.displayBookList(bookList);
    }

    private static class GetBookListUseCaseSubscriber extends SubscriberWithPresenter<List<Book>, BookListPresenter> {

        GetBookListUseCaseSubscriber(BookListPresenter presenter) {
            super(presenter, true);
        }

        @Override
        public void onNext(List<Book> bookList) {
            getPresenter().getView().hideProgress();
            getPresenter().displayBookList(bookList);
        }

    }

}
