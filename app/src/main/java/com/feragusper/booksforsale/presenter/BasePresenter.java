package com.feragusper.booksforsale.presenter;

import android.support.annotation.CallSuper;

import com.feragusper.booksforsale.data.net.RetrofitException;
import com.feragusper.booksforsale.domain.interactor.DefaultSubscriber;
import com.feragusper.booksforsale.domain.interactor.UseCase;
import com.feragusper.booksforsale.view.BaseView;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Presenter with MVP behavior
 */
public class BasePresenter<V extends BaseView> {

    private final Map<String, UseCase> useCasesMap = new HashMap<>();
    protected V view;
    private UseCase lastUseCase;
    private Subscriber lastSubscriber;

    BasePresenter(UseCase... useCases) {
        for (UseCase useCase : useCases) {
            useCasesMap.put(useCase.getCode(), useCase);
        }
    }

    V getView() {
        return view;
    }

    public void setView(V view) {
        this.view = view;
    }

    public void onCreate() {
        //Nothing to do here now
    }

    public void onResume() {
        // Nothing to do here now
    }

    public void onPause() {
        // Nothing to do here now
    }

    @CallSuper
    public void onDestroy() {
        for (UseCase useCase : useCasesMap.values()) {
            useCase.unsubscribe();
        }
    }

    final void executeUseCase(String code, Subscriber subscriber) {
        lastUseCase = useCasesMap.get(code);
        lastSubscriber = subscriber;
        lastUseCase.execute(lastSubscriber);
    }

    public void onViewCreated() {
        // Nothing to do here now
    }

    protected abstract static class SubscriberWithPresenter<T, P extends BasePresenter> extends DefaultSubscriber<T> {

        private final WeakReference<P> viewWeakReference;
        private final boolean hasRetry;

        SubscriberWithPresenter(P presenter, boolean hasRetry) {
            viewWeakReference = new WeakReference<>(presenter);
            this.hasRetry = hasRetry;
        }

        P getPresenter() {
            return viewWeakReference.get();
        }

        @Override
        public final void onError(Throwable throwable) {
            super.onError(throwable);
            if (throwable instanceof RetrofitException) {
                RetrofitException retrofitException = (RetrofitException) throwable;
                if (retrofitException.getKind().equals(RetrofitException.Kind.HTTP)) {
                    switch (retrofitException.getCode()) {
                        default:
                            onRetrofitError(retrofitException);
                            break;
                    }
                } else {
                    onRetrofitError(retrofitException);
                }
            }
        }

        void onRetrofitError(RetrofitException throwable) {
            getPresenter().getView().hideProgress();
            if (hasRetry) {
                getPresenter().getView().showErrorWithRetry(throwable.getMessage());
            } else {
                getPresenter().getView().showError(throwable.getMessage());
            }
        }
    }
}