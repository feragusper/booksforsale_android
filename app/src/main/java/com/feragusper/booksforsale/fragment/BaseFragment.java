package com.feragusper.booksforsale.fragment;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Fragment with common behavior for all kind of fragments used in within the app.
 */
public abstract class BaseFragment extends Fragment {

    @Nullable
    @Override
    @CallSuper
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(makeLayoutResourceId(), container, false);
        ButterKnife.bind(this, fragmentView);
        setRetainInstance(true);
        return fragmentView;
    }

    protected abstract int makeLayoutResourceId();
}
