package com.feragusper.booksforsale.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.feragusper.booksforsale.R;
import com.feragusper.booksforsale.presenter.BasePresenter;
import com.feragusper.booksforsale.view.BaseView;

import javax.inject.Inject;

import butterknife.BindView;
import dagger.android.support.AndroidSupportInjection;

/**
 * @author Fernando Perez
 * @since 0.1
 * Fragment with MVP behavior.
 */
public abstract class MVPFragment<P extends BasePresenter> extends BaseFragment implements BaseView {

    @BindView(R.id.coordinatorLayout)
    protected CoordinatorLayout cl_coordinatorLayout;
    @Inject
    protected P presenter;
    @BindView(R.id.progress)
    RelativeLayout progress;

    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onCreate();
    }

    @SuppressWarnings("unchecked")
    @CallSuper
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.setView(this);
        presenter.onViewCreated();
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @CallSuper
    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onDestroy();
        }
    }

    @CallSuper
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
        onPresenterInjected();
    }

    @Override
    public void showError(String message) {
        Snackbar.make(cl_coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showErrorWithRetry(String message) {
        Snackbar.make(cl_coordinatorLayout, message, Snackbar.LENGTH_INDEFINITE).show();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    protected void onPresenterInjected() {
        // Do nothing by default
    }

}
