package com.feragusper.booksforsale.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.feragusper.booksforsale.R;
import com.feragusper.booksforsale.adapter.BookListAdapter;
import com.feragusper.booksforsale.domain.model.Book;
import com.feragusper.booksforsale.presenter.BookListPresenter;
import com.feragusper.booksforsale.view.BookListView;

import java.util.List;

import butterknife.BindView;

/**
 * @author Fernando Perez
 * @since 1.0
 */
public class BookListFragment extends MVPFragment<BookListPresenter> implements BookListView {

    private static final String LIST_STATE_KEY = "list.state.key";
    @BindView(R.id.book_list)
    RecyclerView bookList;
    private BookListAdapter bookListAdapter;
    private LinearLayoutManager mLayoutManager;
    private Parcelable mListState;

    public static BaseFragment newInstance() {
        return new BookListFragment();
    }

    @Override
    protected int makeLayoutResourceId() {
        return R.layout.fragment_book_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getContext());
        bookList.setLayoutManager(mLayoutManager);

        bookListAdapter = new BookListAdapter();
        bookList.setAdapter(bookListAdapter);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(LIST_STATE_KEY, bookList.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(LIST_STATE_KEY);
        }
    }

    @Override
    public void displayBookList(List<Book> productList) {
        bookListAdapter.updateList(productList);
        if (mListState != null) {
            mLayoutManager.onRestoreInstanceState(mListState);
        }
    }
}
