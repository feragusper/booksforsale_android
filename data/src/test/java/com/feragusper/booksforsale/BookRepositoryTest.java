package com.feragusper.booksforsale;

import com.feragusper.booksforsale.data.net.APIRESTClientService;
import com.feragusper.booksforsale.data.repository.BookRepositoryImplementation;
import com.feragusper.booksforsale.data.repository.cache.BookCache;
import com.feragusper.booksforsale.domain.repository.BookRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import rx.Observable;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SuppressWarnings("deprecation")
@RunWith(MockitoJUnitRunner.class)
public class BookRepositoryTest {

    private BookRepository bookRepository;
    @Mock
    private BookCache cache;
    @Mock
    private APIRESTClientService service;

    @Before
    public void setUp() {
        bookRepository = new BookRepositoryImplementation(service, cache);
    }

    @Test
    public void testGetBooks() {
        given(service.getBooks()).willReturn(Observable.just(new ArrayList<>()));

        bookRepository.getBooks();

        verify(service).getBooks();
    }
}
