package com.feragusper.booksforsale.data.net;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 */
public interface Environment {
    String getBaseURL();
}
