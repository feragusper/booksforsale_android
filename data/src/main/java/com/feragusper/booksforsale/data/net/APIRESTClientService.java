package com.feragusper.booksforsale.data.net;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.feragusper.booksforsale.domain.model.Book;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Service to handle the API calls
 */
public class APIRESTClientService {

    private static final int CONNECTION_TIME_OUT = 10;
    private BooksForSaleAPIREST service;

    @Inject
    public APIRESTClientService(Environment environment) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(environment.getBaseURL())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(makeClient())
                .build();
        service = retrofit.create(BooksForSaleAPIREST.class);
    }

    private OkHttpClient makeClient() {
        HttpLoggingInterceptor.Level logLevel = HttpLoggingInterceptor.Level.BODY;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(logLevel);

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(new StethoInterceptor())
                .writeTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS);
        return builder.build();
    }

    public Observable<List<Book>> getBooks() {
        return service.getBookList();
    }

}
