package com.feragusper.booksforsale.data.repository;

import com.feragusper.booksforsale.data.net.APIRESTClientService;
import com.feragusper.booksforsale.data.repository.cache.BookCache;
import com.feragusper.booksforsale.domain.model.Book;
import com.feragusper.booksforsale.domain.repository.BookRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;


/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Repository to handle Books
 */
@Singleton
public class BookRepositoryImplementation implements BookRepository {

    private final APIRESTClientService service;
    private final BookCache cache;

    @SuppressWarnings("WeakerAccess")
    @Inject
    public BookRepositoryImplementation(APIRESTClientService service, BookCache cache) {
        this.service = service;
        this.cache = cache;
    }

    @Override
    public Observable<List<Book>> getBooks() {
        if (cache.getAll().isEmpty()) {
            return service.getBooks().doOnNext(cache::save);
        }

        return Observable.just(cache.getAll());
    }
}
