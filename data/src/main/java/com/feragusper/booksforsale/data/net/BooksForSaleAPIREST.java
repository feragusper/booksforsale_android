package com.feragusper.booksforsale.data.net;

import com.feragusper.booksforsale.domain.model.Book;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import rx.Observable;

/**
 * @author Fernando Perez
 * @since 1.0
 *
 * Interface that defines the Book For Sale API calls
 */
public interface BooksForSaleAPIREST {

    @Headers("Content-Type: application/json")
    @GET("books.json")
    Observable<List<Book>> getBookList();
}
