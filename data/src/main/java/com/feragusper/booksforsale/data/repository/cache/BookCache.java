package com.feragusper.booksforsale.data.repository.cache;


import com.feragusper.booksforsale.domain.model.Book;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Fernando.Perez
 * @since 1.0
 *
 * Cache for Books
 */
public class BookCache {
    private List<Book> bookList = new ArrayList<>();

    @SuppressWarnings("WeakerAccess")
    @Inject
    public BookCache() {
    }

    public void save(List<Book> bookList) {
        this.bookList.addAll(bookList);
    }

    public List<Book> getAll() {
        return bookList;
    }
}
